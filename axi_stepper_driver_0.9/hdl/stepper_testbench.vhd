library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity stepper_testbench is
--  Port ( );
end stepper_testbench;

architecture behavioral of stepper_testbench is
	component stepper_driver
		port (
			debug_out			: out std_logic_vector(31 downto 0);
			aclk				: in std_logic;
			aresetn				: in std_logic;
			reg_wr_en			: in std_logic;
			stepper_ctrl			: in std_logic_vector(31 downto 0);
			stepper_status			: out std_logic_vector(31 downto 0);
			stepper_output_pulse_width	: in std_logic_vector(31 downto 0);
			stepper_steps_in		: in std_logic_vector(31 downto 0); -- period?
			reset				: in std_logic;
			pop				: in std_logic;
			enablen				: out std_logic;
			step				: out std_logic;
			direction			: out std_logic;
			interrupt			: out std_logic;
			fifo_wr_en			: out std_logic;
			fifo_dout			: out std_logic_vector(31 downto 0);
			fifo_almost_full		: in std_logic;
			fifo_full			: in std_logic;
			fifo_rd_en			: out std_logic;
			fifo_din			: in std_logic_vector(31 downto 0);
			fifo_almost_empty		: in std_logic;
			fifo_empty			: in std_logic;
			fifo_dcnt			: in std_logic_vector(23 downto 0)
		);
	end component;

	signal S_AXI_ACLK	: std_logic;
	signal S_AXI_ARESETN	: std_logic;
	signal slv_reg_wren	: std_logic;
	signal slv_reg0		: std_logic_vector(31 downto 0);
	signal slv_reg1		: std_logic_vector(31 downto 0);
	signal slv_reg2		: std_logic_vector(31 downto 0);
	signal slv_reg3		: std_logic_vector(31 downto 0);
	signal slv_reg4		: std_logic_vector(31 downto 0);
	signal RESET		: std_logic;
	signal POP		: std_logic;
	signal ENABLEN		: std_logic;
	signal STEP		: std_logic;
	signal DIRECTION	: std_logic;
	signal INTERRUPT	: std_logic;
	signal FIFO_DOUT	: std_logic_vector(31 downto 0);
	signal FIFO_DIN		: std_logic_vector(31 downto 0);
	signal FIFO_WR_EN	: std_logic;
	signal FIFO_RD_EN	: std_logic;
	signal FIFO_ALMOST_FULL	: std_logic;
	signal FIFO_ALMOST_EMPTY: std_logic;
	signal FIFO_FULL	: std_logic;
	signal FIFO_EMPTY	: std_logic;
	signal FIFO_DCNT	: std_logic_vector(23 downto 0);
begin

	stepper_driver_0	: stepper_driver
	port map (
		debug_out			=> slv_reg4,
		aclk				=> S_AXI_ACLK,
		aresetn				=> S_AXI_ARESETN,
		stepper_ctrl			=> slv_reg0,
		stepper_status			=> slv_reg1,
		stepper_output_pulse_width	=> slv_reg2,
		stepper_steps_in		=> slv_reg3,
		stepper_steps_in_load		=> slv_reg_wren,
		reset				=> RESET,
		pop				=> POP,
		enablen				=> ENABLEN,
		step				=> STEP,
		direction			=> DIRECTION,
		interrupt			=> INTERRUPT,
		fifo_dout			=> FIFO_DOUT,
		fifo_din			=> FIFO_DIN,
		fifo_wr_en			=> FIFO_WR_EN,
		fifo_rd_en			=> FIFO_RD_EN,
		fifo_almost_full		=> FIFO_ALMOST_FULL,
		fifo_full			=> FIFO_FULL,
		fifo_almost_empty		=> FIFO_ALMOST_EMPTY,
		fifo_empty			=> FIFO_EMPTY,
		fifo_dcnt			=> FIFO_DCNT
	);

	process
	begin
		S_AXI_ACLK <= '1';
		wait for 5 ns;
		S_AXI_ACLK <= '0';
		wait for 5 ns;
	end process;

	process
	begin
		pop <= '1';
		wait for 5 ns;
		pop <= '0';
		wait for 666661 ns;
	end process;

	process
	begin
		S_AXI_ARESETN <= '1';
		reset <= '1';
		slv_reg_wren <= '0';
		slv_reg0 <= "00000000000000000000000000000001";
		slv_reg2 <= "00000000000000000000000111111111";
		slv_reg3 <= "00000000000000000000000000101011";

		wait for 10 ns;
	end process;

end behavioral;
