#!/bin/bash -f
xv_path="/opt/Xilinx/Vivado/2016.2"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xelab -wto ab139fc801da4d9b930b99e7ae01b78b -m64 --debug typical --relax --mt 8 -L xil_defaultlib -L secureip --snapshot axi_clock_generator_v0_9_behav xil_defaultlib.axi_clock_generator_v0_9 -log elaborate.log
