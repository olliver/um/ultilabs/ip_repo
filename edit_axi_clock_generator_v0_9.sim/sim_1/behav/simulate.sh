#!/bin/bash -f
xv_path="/opt/Xilinx/Vivado/2016.2"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xsim axi_clock_generator_v0_9_behav -key {Behavioral:sim_1:Functional:axi_clock_generator_v0_9} -tclbatch axi_clock_generator_v0_9.tcl -log simulate.log
