#!/bin/bash -f
xv_path="/opt/Xilinx/Vivado/2016.2"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xelab -wto 51b173a5faae4cf9bd2f8d15438fd1ad -m64 --debug typical --relax --mt 8 -L xil_defaultlib -L secureip --snapshot stepper_testbench_behav xil_defaultlib.stepper_testbench -log elaborate.log
