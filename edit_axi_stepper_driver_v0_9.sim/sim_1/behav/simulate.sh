#!/bin/bash -f
xv_path="/opt/Xilinx/Vivado/2016.2"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xsim stepper_testbench_behav -key {Behavioral:sim_1:Functional:stepper_testbench} -tclbatch stepper_testbench.tcl -log simulate.log
