library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity stepper_test is
--  Port ( );
end stepper_test;

architecture behavioral of stepper_test is
	component stepper_driver
		port (
			aclk				: in std_logic;
			aresetn				: in std_logic;
			stepper_ctrl			: in std_logic_vector(31 downto 0);
			stepper_fifo			: in std_logic_vector(31 downto 0);
			stepper_output_pulse_width	: in std_logic_vector(31 downto 0);
			stepper_outputs			: out std_logic_vector(31 downto 0);
			reset				: in std_logic;
			pop				: in std_logic;
			enablen				: out std_logic;
			step				: out std_logic;
			direction			: out std_logic;
			interrupt			: out std_logic
		);
	end component;

	signal S_AXI_ACLK	: std_logic;
	signal S_AXI_ARESETN	: std_logic;
	signal slv_reg0		: std_logic_vector(31 downto 0);
	signal slv_reg1		: std_logic_vector(31 downto 0);
	signal slv_reg2		: std_logic_vector(31 downto 0);
	signal slv_reg3		: std_logic_vector(31 downto 0);
	signal RESET		: std_logic;
	signal POP		: std_logic;
	signal ENABLEN		: std_logic;
	signal STEP		: std_logic;
	signal DIRECTION	: std_logic;
	signal INTERRUPT	: std_logic;
begin

	stepper_driver_0	: stepper_driver
	port map (
		aclk				=> S_AXI_ACLK,
		aresetn				=> S_AXI_ARESETN,
		stepper_ctrl			=> slv_reg0,
		stepper_fifo			=> slv_reg1,
		stepper_output_pulse_width	=> slv_reg2,
		stepper_outputs			=> slv_reg3,
		reset				=> RESET,
		pop				=> POP,
		enablen				=> ENABLEN,
		step				=> STEP,
		direction			=> DIRECTION,
		interrupt			=> INTERRUPT
	);

	process
	begin
		S_AXI_ACLK <= '1';
		wait for 5 ns;
		S_AXI_ACLK <= '0';
		wait for 5 ns;
	end process;

	process
	begin
		pop <= '1';
		wait for 5 ns;
		pop <= '0';
		wait for 55 ns;
	end process;

	process
	begin
		S_AXI_ARESETN <= '1';
		reset <= '1';
		slv_reg0 <= "00000000000000000000000000000001";
		slv_reg1 <= "00000000000000000000000000101101";
		slv_reg2 <= "00000000000000000000000000000011";

		wait for 10 ns;
	end process;

end behavioral;
