library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity stepper_driver is
	port (
		aclk			: in std_logic;
		aresetn			: in std_logic;
		stepper_ctrl		: in std_logic_vector(31 downto 0);
		stepper_out		: in std_logic_vector(31 downto 0); -- period?
		stepper_out_pulse_width	: in std_logic_vector(31 downto 0);
		stepper_outputs		: out std_logic_vector(31 downto 0);
		reset			: in std_logic;
		pop			: in std_logic;
		enablen			: out std_logic;
		pulse			: out std_logic;
		direction		: out std_logic;
		interrupt		: out std_logic
	);
end stepper_driver;

architecture behavioral of stepper_driver is

	signal fifo_cnt			: natural range 0 to 15;
	signal interrupt_i		: std_logic;
	signal out_i			: std_logic_vector(31 downto 0);
	signal out_pulse_width_i	: natural range 0 to 511;
	alias enable			: std_logic is stepper_ctrl(0);
	alias output			: std_logic_vector is stepper_out;
	alias output_pulse_width	: std_logic_vector is stepper_out_pulse_width;
	alias outputs			: std_logic_vector is stepper_outputs;

begin

	process (aclk) is
	begin
		if (rising_edge(aclk)) then
			if (reset = '1') then
				out_i <= (others => '0');
				interrupt_i <= '0';
				fifo_cnt <= 0;
				out_pulse_width_i <= 0;
			else
				if (pop = '1') then
					--out_i <= out_i srl 2;
					out_i(31 downto 0) <= b"00" & out_i(31 downto 2); -- right shift by 2

					if (fifo_cnt = 15) then
						fifo_cnt <= 0;
					else
						fifo_cnt <= fifo_cnt + 1;
					end if;
				else
					if (out_pulse_width_i = to_integer(unsigned(out_pulse_width))) then
						out_i(0) <= '0';
						out_i(1) <= '0';
					end if;
					if (fifo_cnt = 0) then
					--	interrupt_i <= '1';
						out_i <= output; -- feed register into buffer
					else
					--	interrupt_i <= '0';
					--	_out <= (others => '0'); TODO multi-driven net, how to get data out via a register?
					end if;
				end if;

				if (out_pulse_width_i = to_integer(unsigned(out_pulse_width))) then 
					out_pulse_width_i <= out_pulse_width_i + 1;
				--	out_i(0) <= '0';
				--	out_i(1) <= '0';
					interrupt_i <= '1';
				else
					out_pulse_width_i <= 0;
					interrupt_i <= '0';
				end if;
			end if;
		end if;
	end process;

	enablen <= (not enable);
	direction <= out_i(0);	-- Even bits in stream are direction bits
	pulse <= out_i(1);	-- Odd bits in stream are pulse bits
	interrupt <= interrupt_i;

end behavioral;
